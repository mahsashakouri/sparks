#!/usr/bin/env python3

from records import Database
from tifffile.tifffile import TiffFile
import pprint

pp = pprint.PrettyPrinter(indent=4)

SQLiteDBName = "/home/markov/.local/share/iocbio/sparks/sparks.sqlite"

sqlite = Database('sqlite:///' + SQLiteDBName)

exp = {}
for r in sqlite.query("SELECT experiment_id, filename FROM experiment"):
    exp[r.experiment_id] = r.filename

sqlite.query("CREATE TABLE IF NOT EXISTS imaging_pars(experiment_id text not null PRIMARY KEY, " +
             "laser DOUBLE PRECISION, gain DOUBLE PRECISION, offset DOUBLE PRECISION, FOREIGN KEY (experiment_id) REFERENCES " +
              "experiment(experiment_id) ON DELETE CASCADE)")

for e in exp:
    with TiffFile(exp[e]) as tif:
        scan_info = tif.lsm_metadata

    laser = scan_info['ScanInformation']['Tracks'][0]['IlluminationChannels'][0]['Power']
    gain = scan_info['ScanInformation']['Tracks'][0]['DetectionChannels'][0]['DetectorGainFirst']
    offset = scan_info['ScanInformation']['Tracks'][0]['DetectionChannels'][0]['AmplifierOffsFirst']
    print(e,laser, gain, offset)

    sqlite.query("INSERT OR REPLACE INTO imaging_pars(experiment_id, laser, gain, offset) VALUES(:eid,:laser,:gain,:offset)",
                 eid=e, laser=laser, gain=gain, offset=offset)
    
    # pp.pprint(scan_info)
    
