from records import Database
from tifffile.tifffile import TiffFile

SQLiteDBName = "/home/markov/.local/share/iocbio/sparks/sparks.sqlite"

sqlite = Database('sqlite:///' + SQLiteDBName)

exp = {}
for r in sqlite.query("SELECT experiment_id, filename FROM experiment"):
    exp[r.experiment_id] = r.filename

for e in exp:
    with TiffFile(exp[e]) as tif:
        scan_info = tif.lsm_metadata

    dx = scan_info['VoxelSizeX']*1e6 # in micrometers
    dt = scan_info['TimeIntervall']*1e3 # in milliseconds
    dimX = scan_info['DimensionX']
    dimTime = scan_info['DimensionTime']

    sqlite.query("UPDATE experiment SET duration=:d, length=:l, pixels_time=:pt, pixels_space=:ps WHERE experiment_id=:eid",
                 eid=e, d=dt*dimTime, l=dx*dimX, pt=dimTime, ps=dimX)

    print(exp[e])
