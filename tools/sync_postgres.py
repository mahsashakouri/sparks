#!/usr/bin/env python3
import getpass
import keyring
import os, sys
import argparse

from records import Database
from PyQt5.QtCore import QCoreApplication, QStandardPaths

# global vars
SQLiteDBName = None # filled later
PostgreSQLTablePrefix = None

def pgtab(name):
    return PostgreSQLTablePrefix + name

def has_record(database, table, **kwargs):
    sql = "SELECT 1 FROM " + table + " WHERE "
    for key in kwargs.keys():
        sql += key + "=:" + key + " AND "
    sql = sql[:-5] # dropping excessive " AND "
    sql += " LIMIT 1"
    for row in database.query(sql, **kwargs):
        return True
    return False

def insert(db, table, row):
    sql = 'INSERT INTO ' + table + "("
    keys = row.keys()
    for k in keys:
        sql += '"' + k + '",'
    sql = sql[:-1] + ") VALUES("
    for k in keys:
        sql += ":" + k + ","
    db.query(sql[:-1] + ")",**row)

def update(db, table, key, row):
    vals = {}
    for k in row.keys():
        if k!= key:
            vals[k] = row[k]

    sql = 'UPDATE ' + table + " SET "
    keys = vals.keys()
    for k in keys:
        sql += '"' + k + '"=:' + k + ","
    sql = sql[:-1] + " WHERE " + key + "=:" + key
    db.query(sql,**row)


def authenticate_sysbio_database(server):
    cli_input = input
    username = getpass.getuser()

    password = keyring.get_password(server, username)

    print('Connecting to {}'.format(server))

    if password is None:
        username = cli_input('  Enter your username: ')
        password = getpass.getpass(prompt='  Enter your password: ')
        save_inp = cli_input('  Save username and password [n/y]: ')

        save = False
        if save_inp == 'y':
            save = True
            keyring.set_password(server, username, password)
            print('Username and password saved.')

        else:
            keyring.delete_password(server, username)
            print('Username and password not saved.')

    return username, password, server


###############################################
## main started
###############################################

# set same paths as the main program
QCoreApplication.setOrganizationName("iocbio")
QCoreApplication.setApplicationName("sparks")
SQLiteDBName = os.path.join(QStandardPaths.writableLocation(QStandardPaths.AppDataLocation),
                            "sparks.sqlite")

parser = argparse.ArgumentParser(description='Synchronize with PostgreSQL database')
parser.add_argument('--sqlite', type=str, default=SQLiteDBName, help='SQLite database file name')
parser.add_argument('--postgres', type=str, default='sysbio-db.kybi/experiments_v2',
                    help='PostgreSQL database name')
parser.add_argument('--prefix', type=str, default='sparks_',
                    help='Prefix for tables in PostgreSQL database')
parser.add_argument('--overwrite', action='store_true',
                    help='If given, uploads new and overwrites existing data in PostgreSQL database by the data from the local database. Otherwise, only experiments absent in PostgreSQL database are uploaded.')
args = parser.parse_args()

SQLiteDBName = args.sqlite
PostgreSQLTablePrefix = args.prefix

database = Database('postgresql://{}:{}@{}'.format(*authenticate_sysbio_database(args.postgres)))
sqlite = Database('sqlite:///' + SQLiteDBName)
overwrite = args.overwrite

if overwrite:
    # for experiments from this database, remove all rois and other
    # dependent data in the target database, but not experiments themself
    # allowing to keep connections between experiment_id and other parts of the
    # database
    exp = []
    for r in sqlite.query("SELECT experiment_id FROM experiment"):
        exp.append(r.experiment_id)

    roi = []
    for r in sqlite.query("SELECT roi_id FROM roi"):
        roi.append(r.roi_id)

    stage = []
    for r in sqlite.query("SELECT stage_id FROM stage"):
        stage.append(r.stage_id)

    # cleanup
    # spark is cleaned automatically via roi table and cascade delete
    # image is one per experiment and will be tackled via upload/update
    for e in exp:
        for r in database.query("SELECT roi_id FROM " + pgtab("roi") + " WHERE experiment_id=:eid", eid=e):
            if r.roi_id not in roi:
                print('Removing ROI:', r.roi_id)
                database.query("DELETE FROM " + pgtab("spark") + " WHERE roi_id=:rid", rid=r.roi_id)

        for r in database.query("SELECT stage_id FROM " + pgtab("stage") + " WHERE experiment_id=:eid", eid=e):
            if r.stage_id not in stage:
                print('Removing Stage:', r.stage_id)
                database.query("DELETE FROM " + pgtab("stage") + " WHERE stage_id=:sid", sid=r.stage_id)

# update or add data
Tables = [
    {'table': 'experiment', 'id': 'experiment_id'},
    {'table': 'roi', 'id': 'roi_id'},
    {'table': 'spark', 'id': 'roi_id' },
    {'table': 'image', 'id': 'experiment_id' },
    {'table': 'stage', 'id': 'stage_id' },
    {'table': 'imaging_pars', 'id': 'experiment_id' },
]

for T in Tables:
    table = T['table']
    key = T['id']
    for r in sqlite.query("SELECT * from " + table):
        eid = r[key]
        if has_record(database, pgtab(table), **{key: eid}):
            if overwrite:
                update(database, pgtab(table), key, r)
                print('Updated:', table, eid)
        else:
            insert(database, pgtab(table),r)
            print('Inserted:', table, eid)

database.close()
